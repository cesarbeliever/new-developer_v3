from fastapi import FastAPI
from pydantic import BaseModel
from typing import List, Tuple
import base64

app = FastAPI()

class Numbers(BaseModel):
    numbers: List[int]
    target_sum: int


def find_matching_pair(numbers: List[int], target_sum: int) -> Tuple[int, int]:
    left_pointer = 0
    right_pointer = len(numbers) - 1

    while left_pointer < right_pointer:
        current_sum = numbers[left_pointer] + numbers[right_pointer]
        if current_sum == target_sum:
            return (numbers[left_pointer], numbers[right_pointer])
        elif current_sum < target_sum:
            left_pointer += 1
        else:
            right_pointer -= 1

    return None

@app.post("/matching-pair/")
async def find_matching_pair_api(numbers: Numbers):
    pair = find_matching_pair(numbers.numbers, numbers.target_sum)
    if pair:
        return {"pair": pair, "message": "Matching pair found."}
    else:
        return {"message": "No matching pair found."}
    
class Item(BaseModel):
    message: str

@app.post("/decode/")
async def decode_message(hex_message: Item):
    decoded_message = bytes.fromhex(str(hex_message.message)).decode('latin-1')
    return {"mensaje": decoded_message}

@app.post("/decode_base64/")
async def decode_base64(encoded_string: Item):
    try:
        decoded_string = base64.b64decode(encoded_string.message).decode('latin-1')
        return {"mensaje": decoded_string}
    except Exception as e:
        return {"error": f"Error decoding string: {str(e)}"}