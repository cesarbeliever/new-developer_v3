import unittest
from fastapi.testclient import TestClient
from code_1 import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_find_matching_pair(self):
        numbers = [2, 3, 6, 7]
        target_sum = 9
        response = self.client.post("/matching-pair/", json={"numbers": numbers, "target_sum": target_sum})
        data = response.json()
        print(data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["message"], "Matching pair found.")
        self.assertEqual(data["pair"], [2, 7])
        return(data)

    def test_decode_message(self):
        hex_message = "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220617175ed212e"
        response = self.client.post("/decode/", json={"message": hex_message})
        data = response.json()
        print(data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["mensaje"], "Este es el último paso, por favor, agregame al hangout: \r\n\r\n\"martin@mendozadelsolar.com\" para saber que llegaste a esta parte.\r\n\r\nGracias, y espero verte por aquí!.")
        return(data)

    def test_decode_base64(self):
        encoded_string = "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu"
        response = self.client.post("/decode_base64/", json={"message": encoded_string})
        data = response.json()
        print(data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["mensaje"], "Si llegas hasta esta parte, por favor, comentamelo con un mensaje.")

if __name__ == '__main__':
    unittest.main()